// QRCODE reader Copyright 2011 Lazar Laszlo
// http://www.webqr.com

var gCtx = null;
var gCanvas = null;
var c=0;
var stype=0;
var gUM=false;
var webkit=false;
var moz=false;
var v=null;

var imghtml = '<div id="qrfile"><canvas id="out-canvas" width="150" height="150"></canvas>' +
    '<input type="file" onchange="handleFiles(this.files)"/>' +
    '</div>' +
    '</div>';

var vidhtml = '<video id="v" autoplay></video>';

function dragenter(e) {
    e.stopPropagation();
    e.preventDefault();
}

function dragover(e) {
    e.stopPropagation();
    e.preventDefault();
}
function drop(e) {
    e.stopPropagation();
    e.preventDefault();

    var dt = e.dataTransfer;
    var files = dt.files;
    if(files.length>0)
    {
        handleFiles(files);
    }
    else
    if(dt.getData('URL'))
    {
        qrcode.decode(dt.getData('URL'));
    }
}

function handleFiles(f)
{
    var o=[];

    for(var i =0;i<f.length;i++)
    {
        var reader = new FileReader();
        reader.onload = (function(theFile) {
            return function(e) {
                gCtx.clearRect(0, 0, gCanvas.width, gCanvas.height);

                qrcode.decode(e.target.result);
            };
        })(f[i]);
        reader.readAsDataURL(f[i]);
    }
}

function initCanvas(w,h)
{
    gCanvas = document.getElementById("qr-canvas");
    gCanvas.style.width = w + "px";
    gCanvas.style.height = h + "px";
    gCanvas.width = w;
    gCanvas.height = h;
    gCtx = gCanvas.getContext("2d");
    gCtx.clearRect(0, 0, w, h);
}


function captureToCanvas() {
    if(stype!=1)
        return;
    if(gUM)
    {
        try{
            gCtx.drawImage(v,0,0);
            try{
                qrcode.decode();
            }
            catch(e){
                console.log(e);
                setTimeout(captureToCanvas, 500);
            };
        }
        catch(e){
            console.log(e);
            setTimeout(captureToCanvas, 500);
        };
    }
}

function htmlEntities(str) {
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}

function read(a)
{
    var html="<br>";
    if(a.indexOf("http://") === 0 || a.indexOf("https://") === 0)
        html+="<a target='_blank' href='"+a+"'>"+a+"</a><br>";
    html+="<b>"+htmlEntities(a)+"</b><br><br>";
    document.getElementById("result").innerHTML=html;
}

function isCanvasSupported(){
    var elem = document.createElement('canvas');
    return !!(elem.getContext && elem.getContext('2d'));
}
function success(stream) {
    if(webkit)
        v.src = window.URL.createObjectURL(stream);
    else
    if(moz)
    {
        v.mozSrcObject = stream;
        v.play();
    }
    else
        v.src = stream;
    gUM=true;
    setTimeout(captureToCanvas, 500);
}

function error(error) {
    gUM=false;
    return;
}

function load()
{
    if(isCanvasSupported() && window.File && window.FileReader)
    {
        initCanvas(150, 150);
        qrcode.callback = read;
    }
    var FormSelector_wrapper = jQuery('.slide-searchbar');
    if (FormSelector_wrapper.length) {
        var FormSelector = jQuery('.slide-searchbar #searchForm');
        action = FormSelector.data('action');
        cover = '<span class="cover-text">' + FormSelector.find('span.cover-text')[0].innerText + '</span>';
        cover2 = '<span>' + FormSelector.find('span.cover-text2')[0].innerText + '</span>';
        buttonSearch = '<button type="submit" title="Search" class="action search"><span>' + FormSelector.find('span.button-search')[0].innerText + '</span></button>';
        input = jQuery('<input type="text" name="q" tabindex="0" class="input-text" id="scanValue" autocomplete="off" />');
        input_type = jQuery('<input type="hidden" name="t" tabindex="0" class="input-text" value="qr" autocomplete="off" />');
        form = jQuery('<form/>');
        form.attr('id', 'searchForm');
        form.attr('action', action);
        form.attr('method', 'get');
        form.attr('class', 'inner-search');
        form.append(cover);
        form.append(cover2);
        form.append(input);
        form.append(input_type);
        form.append(buttonSearch);
        FormSelector_wrapper.html(form);
    }
}

function setwebcam()
{

    var options = true;
    if(navigator.mediaDevices && navigator.mediaDevices.enumerateDevices)
    {
        try{
            navigator.mediaDevices.enumerateDevices()
                .then(function(devices) {
                    devices.forEach(function(device) {
                        if (device.kind === 'videoinput') {
                            if(device.label.toLowerCase().search("back") >-1)
                                options={'deviceId': {'exact':device.deviceId}, 'facingMode':'environment'} ;
                        }
                        console.log(device.kind + ": " + device.label +" id = " + device.deviceId);
                    });
                    setwebcam2(options);
                });
        }
        catch(e)
        {
            console.log(e);
        }
    }
    else{
        console.log("no navigator.mediaDevices.enumerateDevices" );
        setwebcam2(options);
    }

}

function setwebcam2(options)
{
    console.log(options);
    document.getElementById("scanValue").innerHTML="- scanning -";
    if(stype==1)
    {
        setTimeout(captureToCanvas, 500);
        return;
    }
    var n=navigator;
    document.getElementById("outdiv").innerHTML = vidhtml;
    v=document.getElementById("v");


    if(n.getUserMedia)
    {
        webkit=true;
        n.getUserMedia({video: options, audio: false}, success, error);
    }
    else
    if(n.webkitGetUserMedia)
    {
        webkit=true;
        n.webkitGetUserMedia({video:options, audio: false}, success, error);
    }
    else
    if(n.mozGetUserMedia)
    {
        moz=true;
        n.mozGetUserMedia({video: options, audio: false}, success, error);
    }
    //
    // document.getElementById("qrimg").style.opacity=0.2;
    // document.getElementById("webcamimg").style.opacity=1.0;

    stype=1;
    setTimeout(captureToCanvas, 500);
}
function setimg() {
    document.getElementById("scanValue").innerHTML = "";
    if (stype == 2)
        return;
    document.getElementById("outdivfile").innerHTML = imghtml;
    var qrfile = document.getElementById("qrfile");
    qrfile.addEventListener("dragenter", dragenter, false);
    qrfile.addEventListener("dragover", dragover, false);
    qrfile.addEventListener("drop", drop, false);
    stype = 2;
}