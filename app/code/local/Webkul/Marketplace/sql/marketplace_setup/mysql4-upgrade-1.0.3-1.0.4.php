<?php
$installer = $this;
$installer->startSetup();

$prefix = Mage::getConfig()->getTablePrefix();

$connection = $this->getConnection();
/**
 * Update tables 'marketplace_saleslist'
 */
$connection->addColumn($prefix.'marketplace_saleslist', 'is_shipping', 'int(2) NOT NULL DEFAULT 0');
$connection->addColumn($prefix.'marketplace_saleslist', 'is_coupon', 'int(2) NOT NULL DEFAULT 0');
$connection->addColumn($prefix.'marketplace_saleslist', 'applied_coupon_amount', 'float(12,4) NOT NULL DEFAULT 0');
$connection->addColumn($prefix.'marketplace_saleslist', 'is_paid', 'int(2) NOT NULL DEFAULT 0');
$connection->addColumn($prefix.'marketplace_saleslist', 'commission_rate', 'int(11) NOT NULL DEFAULT 0');
/**
 * Update tables 'marketplace_orders'
 */
$connection->addColumn($prefix.'marketplace_orders', 'refunded_shipping_charges', 'float(12,4) NOT NULL DEFAULT 0');
$connection->addColumn($prefix.'marketplace_orders', 'total_tax', 'float(12,4) NOT NULL DEFAULT 0');
$connection->addColumn($prefix.'marketplace_orders', 'coupon_amount', 'float(12,4) NOT NULL DEFAULT 0');
$connection->addColumn($prefix.'marketplace_orders', 'refunded_coupon_amount', 'float(12,4) NOT NULL DEFAULT 0');
$connection->addColumn($prefix.'marketplace_orders', 'tax_to_seller', 'int(2) NOT NULL DEFAULT 0');

$installer->endSetup(); 
