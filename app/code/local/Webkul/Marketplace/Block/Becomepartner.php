<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Marketplace
 * @author    Webkul
 * @copyright Copyright (c) 2010-2016 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
class Webkul_Marketplace_Block_Becomepartner extends Mage_Core_Block_Template
{
	public function __construct(){		
		parent::__construct();
		$partnerId=Mage::getSingleton('customer/session')->getCustomerId(); 
		$collection=Mage::getModel('marketplace/userprofile')->getCollection(); 
		$collection->addFieldToFilter('mageuserid',array('eq'=>$partnerId)); 
		$collection->addFieldToFilter('wantpartner',array('eq'=>1)); 
		if(count($collection)==1){
			$url=$this->getUrl('marketplace/marketplaceaccount/simpleproduct/');
			Mage::app()->getFrontController()->getResponse()->setRedirect($url);
		}
	}
	public function _prepareLayout()
 {
		return parent::_prepareLayout();
    }
}