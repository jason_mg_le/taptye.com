<?php
/**
 * Webkul Marketplace Product Block
 *
 * @category    Webkul
 * @package     Webkul_Marketplace
 * @author      Webkul Software Private Limited
 *
 */
class Webkul_Marketplace_Block_Marketplace extends Mage_Customer_Block_Account_Dashboard
{
	/**
     * Seller's Product Collection
     *
     * @var Mage_Eav_Model_Entity_Collection_Abstract
     */
	protected $_productsCollection = null;

	public function __construct(){		
		parent::__construct();	
		$paramData = $this->getRequest()->getParams();
		$filter = '';
		$filterType = '';
		$filter_prostatus = '';
		$filter_data_frm = '';
		$filter_data_to = '';
		$from = null;
		$to = null;
    	$userId=Mage::getSingleton('customer/session')->getCustomer()->getId();
		$collection = Mage::getModel('marketplace/product')->getCollection()->addFieldToFilter('userid',array('eq'=>$userId));
		$products=array();
		foreach($collection as $data){
			array_push($products,$data->getMageproductid());
		}
		if(isset($paramData['s'])){
            $filter = $paramData['s'] != ""?$paramData['s']:"";
		}
		if(isset($paramData['type'])){
	        $filterType = $paramData['type'] != "" ? $paramData['type']: "";
		}
		if(isset($paramData['prostatus'])){
            $filter_prostatus = $paramData['prostatus'] != ""?$paramData['prostatus']:"";
		}
		if(isset($paramData['from_date'])){
            $filter_data_frm = $paramData['from_date'] != ""?$paramData['from_date']:"";
		}
		if(isset($paramData['to_date'])){
            $filter_data_to = $paramData['to_date'] != ""?$paramData['to_date']:"";
		}
		if($filter_data_to){
			$todate = date_create($filter_data_to);
			$to = date_format($todate, 'Y-m-d 23:59:59');
		}
		if($filter_data_frm){
			$fromdate = date_create($filter_data_frm);
			$from = date_format($fromdate, 'Y-m-d H:i:s');
		}

		$alloweds=explode(',',Mage::helper('marketplace')->getAllowedProductType());
		$allowedproducts=array();
		foreach($alloweds as $allowed){
			array_push($allowedproducts,$allowed);
		}

		$collection = Mage::getModel('catalog/product')->getCollection()
						   ->addAttributeToSelect('*');
		if ($filterType) {
			$collection->addFieldToFilter('type_id', $filterType);
		} else {
			$collection->addFieldToFilter('type_id', array('in'=>$allowedproducts));
		}
		if ($filter) {
			$collection->addFieldToFilter('name', array('like'=>"%".$filter."%"));
		}
		if($filter_prostatus){
			$collection->addFieldToFilter('status', array('like'=>"%".$filter_prostatus."%"));
		}
						  
		$collection->addFieldToFilter('created_at', array('datetime' => true,'from' => $from,'to' =>  $to))
				   ->addFieldToFilter('entity_id', array('in'=>$products))
				   ->setOrder('entity_id','DESC');
		$this->setCollection($collection);
	}
	protected function _prepareLayout() {
        parent::_prepareLayout(); 
        $pager = $this->getLayout()->createBlock('page/html_pager', 'custom.pager');
        $grid_per_page_values = explode(",",Mage::helper('marketplace')->getCatatlogGridPerPageValues());
        $arr_perpage = array();
        foreach ($grid_per_page_values as $value) {
        	$arr_perpage[$value] = $value;
        }
        $pager->setAvailableLimit($arr_perpage);
        $pager->setCollection($this->getCollection());
        $this->setChild('pager', $pager);
        $this->getCollection()->load();
        return $this;
    } 
	
	/**
     * @return pager html block
     */
    public function getPagerHtml() {
        return $this->getChildHtml('pager');
    }

    /**
     * Retrieve current product model instance
     *
     * @return Mage_Catalog_Model_Product
     */
	public function getProduct() {
		$id = $this->getRequest()->getParam('id');
		$products = Mage::getModel('catalog/product')->load($id);
		return $products;
	}

	public function getAllowedProductTypes(){
		$alloweds=explode(',',Mage::helper('marketplace')->getAllowedProductType());
		$helper = Mage::helper('marketplace');
		$data =  array('simple'=>$helper->__('Simple'),
						'downloadable'=>$helper->__('Downloadable'),
					    'virtual'=>$helper->__('Virtual'),
						'configurable'=>$helper->__('Configurable'),
						'grouped'=>$helper->__('Grouped Product'),
						'bundle'=>$helper->__('Bundle Product')
			);
		$allowedproducts=array();
		foreach($alloweds as $allowed){
			array_push($allowedproducts,array('value'=>$allowed, 'label'=>$data[$allowed]));
		}
		return $allowedproducts;
	}
}
