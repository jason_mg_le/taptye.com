<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Marketplace
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
class Webkul_Marketplace_Block_LayoutOne extends Mage_Core_Block_Template
{
    public function getAllStores()
    {
    	return Mage::app()->getStores();
    }

    public function getStoreId()
    {
    	return Mage::app()->getStore()->getStoreId();
    }

    public function getStore()
    {
    	return Mage::app()->getStore();
    }

    public function getSellerArray()
    {
    	$helper = Mage::helper('marketplace');
    	$allStores = $this->getAllStores();
		if(count($allStores)>1){
			$store_id = $this->getStoreId();
		}else{
			$store_id = 0;
		}
		$store_id = 0;

    	$visibility_attribute_id = Mage::getModel("eav/entity_attribute")->loadByCode("catalog_product", "visibility")->getAttributeId();
		$status_attribute_id = Mage::getModel("eav/entity_attribute")->loadByCode("catalog_product", "status")->getAttributeId();

		/*order collection*/

		$sellers_order = Mage::getModel('marketplace/order')->getCollection()
		                ->addFieldToFilter('invoice_id',array('neq'=>0))
		                ->addFieldToSelect('seller_id');
		$prefix = Mage::getConfig()->getTablePrefix();
		$sellers_order->getSelect()
		        ->join(array("ccp" => $prefix."marketplace_userdata"),"ccp.mageuserid = main_table.seller_id",array("wantpartner" => "wantpartner"))
		        ->where("ccp.wantpartner = 1");
		$sellers_order ->getSelect()
		                ->columns('COUNT(*) as countOrder')
		                ->group('seller_id');

		$seller_arr = array();
		foreach ($sellers_order as $value) {
			if($helper->getSellerProCount($value['seller_id'])){
				$seller_arr[$value['seller_id']] = array();
				$seller_products = Mage::getModel('marketplace/saleslist')->getCollection()
				                ->addFieldToFilter('mageproownerid',array('eq'=>$value['seller_id']))
				                ->addFieldToFilter('cpprostatus',array('eq'=>1))
				                ->addFieldToSelect('mageproid')
				                ->addFieldToSelect('magequantity');
				$seller_products ->getSelect()
				                ->columns('SUM(magequantity) as countOrderedProduct')
				                ->group('mageproid');
				$seller_products->getSelect()
		        			->join(array("ccp" => $prefix."marketplace_product"),"ccp.mageproductid = main_table.mageproid",array("status" => "status"))->where("ccp.status = 1");
				$seller_products->setOrder('countOrderedProduct', 'DESC')->setPageSize(3);
				foreach ($seller_products as $seller_product) {
					array_push($seller_arr[$value['seller_id']], $seller_product['mageproid']);
				}
			}
		}
		if(count($seller_arr)!=4){
			$i = count($seller_arr);
			$count_pro_arr = array();
			$seller_product_coll = Mage::getModel('marketplace/product')->getCollection()
										->addFieldToFilter('status',array('eq'=>1));
			/* join with catalog product */
			$seller_product_coll->getSelect()
								->join(array("cp" => $prefix."catalog_product_entity"),"cp.entity_id = main_table.mageproductid");					
			$seller_product_coll->getSelect()
								->join(array("cep" => $prefix."catalog_product_entity_int"),"cep.entity_id = main_table.mageproductid",array("visibility" => "value"))
								->where("cep.attribute_id = ".$visibility_attribute_id." AND cep.store_id = ".$store_id);

			$seller_product_coll->addFilterToMap("visibility","cep.value");
			$seller_product_coll->addFieldToFilter('visibility',array('eq'=>4));
			$seller_product_coll->getSelect()
								->join(array("csp" => $prefix."catalog_product_entity_int"),"csp.entity_id = main_table.mageproductid",array("status" => "value"))
								->where("csp.attribute_id = ".$status_attribute_id." AND csp.store_id = ".$store_id);

			$seller_product_coll->addFilterToMap("status","csp.value");
			$seller_product_coll->addFieldToFilter('status',array('eq'=>1));
			/* join with marketplace_userdata table */
			$seller_product_coll->getSelect()
						        ->join(array("ccp" => $prefix."marketplace_userdata"),"ccp.mageuserid = main_table.userid",array("wantpartner" => "wantpartner"))
						        ->where("ccp.wantpartner = 1");

			$seller_product_coll->getSelect()
						        ->columns('COUNT(*) as countOrder')
						        ->group('userid'); 
        
			foreach ($seller_product_coll as $value) {
				if(!isset($count_pro_arr[$value['userid']])){
					$count_pro_arr[$value['userid']] = array();
				}
				$count_pro_arr[$value['userid']] = $value['countOrder'];
			}	

			arsort($count_pro_arr);

			foreach ($count_pro_arr as $procount_seller_id=>$procount) {		
				if($i<=4){
					if($helper->getSellerProCount($procount_seller_id)){
						if(!isset($seller_arr[$procount_seller_id])){
							$seller_arr[$procount_seller_id] = array();
						}
						$seller_product_coll = Mage::getModel('marketplace/product')->getCollection()
												->addFieldToFilter('userid',array('eq'=>$procount_seller_id))
												->addFieldToFilter('status',array('eq'=>1));
						/* join with catalog product */
						$seller_product_coll->getSelect()
											->join(array("cp" => $prefix."catalog_product_entity"),"cp.entity_id = main_table.mageproductid");
						$seller_product_coll->getSelect()
											->join(array("cep" => $prefix."catalog_product_entity_int"),"cep.entity_id = main_table.mageproductid",array("visibility" => "value"))
											->where("cep.attribute_id = ".$visibility_attribute_id." AND cep.store_id = ".$store_id);
						$seller_product_coll->addFilterToMap("visibility","cep.value");
						$seller_product_coll->addFieldToFilter('visibility',array('eq'=>4));
						$seller_product_coll->getSelect()
											->join(array("csp" => $prefix."catalog_product_entity_int"),"csp.entity_id = main_table.mageproductid",array("status" => "value"))
											->where("csp.attribute_id = ".$status_attribute_id." AND csp.store_id = ".$store_id);
						$seller_product_coll->addFilterToMap("status","csp.value");
						$seller_product_coll->addFieldToFilter('status',array('eq'=>1));
						$seller_product_coll->setPageSize(3);
						foreach ($seller_product_coll as $value) {
							array_push($seller_arr[$procount_seller_id],$value['mageproductid']);
						}
					}	
				}
				$i++;
			}
			return $seller_arr;
		}
    }

    public function laodProduct($productId)
    {
    	return Mage::getModel('catalog/product')->load($productId);
    }

    public function getSellerData($sellerId, $currentStoreId)
    {
		$seller_data = Mage::getModel('marketplace/userprofile')->getCollection()
			->addFieldToFilter('mageuserid',array('eq'=>$sellerId))
			->addFieldToFilter('store_id',array('eq'=>$currentStoreId));
		if(!count($seller_data)){
		    $seller_data = Mage::getModel('marketplace/userprofile')->getCollection()
		    ->addFieldToFilter('mageuserid', $sellerId)
		    ->addFieldToFilter('store_id', 0);
		}
		return $seller_data;
    }
}
