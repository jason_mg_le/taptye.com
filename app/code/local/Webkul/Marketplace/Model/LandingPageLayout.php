<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Marketplace
 * @author    Webkul
 * @copyright Copyright (c) 2010-2016 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

class Webkul_Marketplace_Model_LandingPageLayout
{
    /**
     * Landing Page Layout options.
     * Options getter.
     *
     * @return array
     */
    public function toOptionArray()
    {
        $data = array(
            array('value' => '1', 'label' => __('Layout 1')),
            array('value' => '2', 'label' => __('Layout 2')),
            array('value' => '3', 'label' => __('Layout 3'))
        );
        return $data;
    }
}
