<?php

/**
* Webkul Software.
*
* @category  Webkul
* @package   Webkul_Marketplace
* @author    Webkul
* @copyright Copyright (c) 2010-2016 Webkul Software Private Limited (https://webkul.com)
* @license   https://store.webkul.com/license.html
*/

class Webkul_Marketplace_Model_System_Config_Validateextension extends Mage_Core_Model_Config_Data
{
    /**
    * Validate extension before saving
    *
    * @return Marketplace_Model_System_Config_Validateextension
    */
    protected function _beforeSave()
    {
        $helper = Mage::helper('core');
        $extensions = $helper->getProtectedFileExtensions();
        $value = $this->getValue();
        if (trim($value) != '') {
            $extensionArr = explode(",", $value);
            $invalidArr = array_intersect($extensionArr, $extensions);
            if (count($invalidArr)>0) {
                throw new Exception(Mage::helper('core')->__('File with an extension %s is protected and cannot be uploaded', implode(",", $invalidArr)));
            }
        }
    }
}
