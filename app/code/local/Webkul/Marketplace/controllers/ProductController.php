<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Marketplace
 * @author    Webkul
 * @copyright Copyright (c) 2010-2016 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
require_once 'Mage/Customer/controllers/AccountController.php';
class Webkul_Marketplace_ProductController extends Mage_Customer_AccountController
{

    /**
     * Related product grid
     */
    public function RelatedAction()
    {
        $this->loadLayout();
        $this->renderLayout();
	}

    /**
     * Up-sells product grid
     */
    public function UpsellAction()
    {
        $this->loadLayout();
        $this->renderLayout();
	}

    /**
     * Cross-sells product grid
     */
    public function CrosssellAction()
    {
        $this->loadLayout();
        $this->renderLayout();
	}

	public function SetConfigurableAttributeAction()
	{
		try{
			$id = (int) $this->getRequest()->getParam('id');

			$sellerId=Mage::getSingleton('customer/session')->getCustomerId();

			$collectionProduct = Mage::getModel('marketplace/product')->getCollection()
			->addFieldToFilter('mageproductid', $id)
			->addFieldToFilter('userid', $sellerId);

			if(count($collectionProduct)){

				$storeId=Mage::app()->getStore()->getStoreId();

				$wholedata = $this->getRequest()->getParams();

				Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

				$magentoProductModel = Mage::getModel('catalog/product')->setStoreId($storeId)->load($id);

				$attr = $wholedata['superattributeselect'];

				$attributeId = 0;

				if(isset($attr[0]) && $attr[0]){
					$attributeId = Mage::getResourceModel('eav/entity_attribute')->getIdByCode('catalog_product',$attr[0]);
				}
				 
				if($attributeId){
					$magentoProductModel->getTypeInstance()->setUsedProductAttributeIds(array($attributeId));
				}
				if (array_key_exists('asso_pro', $wholedata)) {
					$asspro = $wholedata['asso_pro'];
					$data[$asspro] = array();
				}
				$i = 0;	 
				$configurable_attributes_data = '';
				foreach($attr as $attrCode){
					if($attrCode){
				        $super_attribute= Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product',$attrCode);
				        $configurableAtt = Mage::getModel('catalog/product_type_configurable_attribute')->setProductAttribute($super_attribute);
				 		$configurable_attributes_data[] = array(
							'id'             => $configurableAtt->getId(),
							'label'          => $configurableAtt->getLabel(),
							'use_default'    => "0",
							'position'       => $super_attribute->getPosition(),
							'values'         => $configurableAtt->getPrices() ? $configProduct->getPrices() : array(),
							'attribute_id'   => $super_attribute->getId(),
							'attribute_code' => $super_attribute->getAttributeCode(),
							'frontend_label' => $super_attribute->getFrontend()->getLabel(),
							"store_label"	 => $super_attribute->getFrontend()->getLabel(),
				        );
				 		$i++;
				 	}
			    }
			    /**
		         * Initialize data for configurable product
		         */
		        if ($configurable_attributes_data) {
		            $magentoProductModel->setConfigurableAttributesData($configurable_attributes_data);
		        }
		        $affect_configurable_product_attributes = 1;
		        $magentoProductModel->setCanSaveConfigurableAttributes($affect_configurable_product_attributes);
		        $magentoProductModel->save();		        

				Mage::app()->setCurrentStore($storeId);

	            $this->_redirect('marketplace/marketplaceaccount/editapprovedconfigurable', array('id'=>$id));
	        } else {
		        $this->_redirect('marketplace/marketplaceaccount/myproductslist');
		    }
		} catch (Exception $e) {
            Mage::logException($e);
            $this->_getSession()->addError($e->getMessage());
            $this->_redirect('marketplace/marketplaceaccount/new/');
        }
	}

    /**
     * Create product duplicate
     */
    public function duplicateAction()
    {
        try {
        	$productId = (int) $this->getRequest()->getParam('id');

			$sellerId=Mage::getSingleton('customer/session')->getCustomerId();

			$collectionProduct = Mage::getModel('marketplace/product')->getCollection()
			->addFieldToFilter('mageproductid', $productId)
			->addFieldToFilter('userid', $sellerId);

			if(count($collectionProduct)){
				$storeId=Mage::app()->getStore()->getStoreId();

				Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);	

	        	$product = $this->_initProduct();

	            $newProduct = $product->duplicate();

				Mage::app()->setCurrentStore($storeId);

	            $this->_getSession()->addSuccess($this->__('The product has been duplicated.'));
	            $id = $newProduct->getId();
	            if ($id) {
					$sellerId = Mage::getSingleton('customer/session')->getCustomer()->getId();
	            	$collection1=Mage::getModel('marketplace/product');
					$collection1->setMageproductid($id);
					$collection1->setUserid($sellerId);
					$collection1->setStatus(2);
					$collection1->save();
	            }
	            $typeId = $newProduct->getTypeId();
	            if ($typeId == 'simple') {
	            	$this->_redirect('marketplace/marketplaceaccount/editapprovedsimple', array('id'=>$id));
	            } else if ($typeId == 'virtual') {
	            	$this->_redirect('marketplace/marketplaceaccount/editapprovedvirtual', array('id'=>$id));
	            } else if ($typeId == 'downloadable') {
	            	$this->_redirect('marketplace/marketplaceaccount/editapproveddownloadable', array('id'=>$id));
	            } else if ($typeId == 'configurable') {
	            	$this->_redirect('marketplace/marketplaceaccount/editapprovedconfigurable', array('id'=>$id));
	            } else {
	            	$this->_redirect('marketplace/marketplaceaccount/new/');
	            }
	        } else {
	        	$this->_redirect('marketplace/marketplaceaccount/myproductslist');
	        }
        } catch (Exception $e) {
            Mage::logException($e);
            $this->_getSession()->addError($e->getMessage());
            $this->_redirect('marketplace/marketplaceaccount/new/');
        }
    }

    /**
     * Initialize product from request parameters
     *
     * @return Mage_Catalog_Model_Product
     */
    protected function _initProduct()
    {
        $productId  = (int) $this->getRequest()->getParam('id');
        $product    = Mage::getModel('catalog/product')
            ->setStoreId($this->getRequest()->getParam('store', 0));

        $product->setData('_edit_mode', true);
        if ($productId) {
            try {
                $product->load($productId);
            } catch (Exception $e) {
                $product->setTypeId(Mage_Catalog_Model_Product_Type::DEFAULT_TYPE);
                Mage::logException($e);
            }
        }
        return $product;
    }
}
