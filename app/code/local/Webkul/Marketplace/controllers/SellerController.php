<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Marketplace
 * @author    Webkul
 * @copyright Copyright (c) 2010-2016 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
class Webkul_Marketplace_SellerController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
		$this->loadLayout();
		$this->renderLayout();
    }

    public function listAction(){
    	if (!Mage::helper('marketplace')->getSellerProfileDisplayFlag()) {
            $this->getRequest()->initForward();
            $this->getRequest()->setActionName('noroute');
            $this->getRequest()->setDispatched(false);
            return false;
        }
		$marketplacelabel=Mage::helper('marketplace')->getMarketplaceHeadLabel();
		$this->loadLayout(array('default','marketplace_seller_list'));
		$this->getLayout()->getBlock('head')->setTitle( Mage::helper('marketplace')->__($marketplacelabel));
		$this->renderLayout();
    }

	public function profileAction(){
		if (!Mage::helper('marketplace')->getSellerProfileDisplayFlag()) {
            $this->getRequest()->initForward();
            $this->getRequest()->setActionName('noroute');
            $this->getRequest()->setDispatched(false);
            return false;
        }
		$id = 0;
		$profileurl = Mage::helper('marketplace')->getProfileUrl();
		if($profileurl){
			$storeId = Mage::app()->getStore()->getId();
			$data=Mage::getModel('marketplace/userprofile')->getCollection()
						->addFieldToFilter('wantpartner',array('eq'=>1))
						->addFieldToFilter('profileurl',array('eq'=>$profileurl))
						->addFieldToFilter('store_id',array('eq'=>$storeId));
            if(!count($data)){
			    $data = Mage::getModel('marketplace/userprofile')->getCollection()
				->addFieldToFilter('wantpartner',array('eq'=>1))
                ->addFieldToFilter('profileurl',array('eq'=>$profileurl))
			    ->addFieldToFilter('store_id', 0);
			}
			foreach($data as $seller){ 
				$id = $seller->getAutoid();
			}
		}
		if($id){
			$this->loadLayout();     
			$this->renderLayout();
		}else{
			$this->_redirect("marketplace/index");
		}		
	}
	public function collectionAction(){
		if (!Mage::helper('marketplace')->getSellerProfileDisplayFlag()) {
            $this->getRequest()->initForward();
            $this->getRequest()->setActionName('noroute');
            $this->getRequest()->setDispatched(false);
            return false;
        }
		$id = 0;		
		$profileurl = Mage::helper('marketplace')->getCollectionUrl();
		if($profileurl){
			$storeId = Mage::app()->getStore()->getId();
			$data=Mage::getModel('marketplace/userprofile')->getCollection()
						->addFieldToFilter('wantpartner',array('eq'=>1))
						->addFieldToFilter('profileurl',array('eq'=>$profileurl))
						->addFieldToFilter('store_id',array('eq'=>$storeId));
            if(!count($data)){
			    $data = Mage::getModel('marketplace/userprofile')->getCollection()
						->addFieldToFilter('wantpartner',array('eq'=>1))
						->addFieldToFilter('profileurl',array('eq'=>$profileurl))
						->addFieldToFilter('store_id', 0);
			}
			foreach($data as $seller){ 
				$id =$seller->getAutoid();
			}
		}
		if($id){
			$this->loadLayout();     
			$this->renderLayout();
		}else{
			$this->_redirect("marketplace/index");
		}
	}
	public function locationAction(){
		if (!Mage::helper('marketplace')->getSellerProfileDisplayFlag()) {
            $this->getRequest()->initForward();
            $this->getRequest()->setActionName('noroute');
            $this->getRequest()->setDispatched(false);
            return false;
        }
		$id = 0;
		$profileurl = Mage::helper('marketplace')->getLocationUrl();
		if($profileurl){
			$storeId = Mage::app()->getStore()->getId();
			$data=Mage::getModel('marketplace/userprofile')->getCollection()
						->addFieldToFilter('wantpartner',array('eq'=>1))
						->addFieldToFilter('profileurl',array('eq'=>$profileurl))
						->addFieldToFilter('store_id',array('eq'=>$storeId));
            if(!count($data)){
			    $data = Mage::getModel('marketplace/userprofile')->getCollection()
						->addFieldToFilter('wantpartner',array('eq'=>1))
						->addFieldToFilter('profileurl',array('eq'=>$profileurl))
						->addFieldToFilter('store_id', 0);
			}
			foreach($data as $seller){ 
				$id =$seller->getAutoid();
			}
		}
		if($id){
			$this->loadLayout();     
			$this->renderLayout();
		}else{
			$this->_redirect("marketplace/index");
		}
	}
	public function feedbackAction(){
		if (!Mage::helper('marketplace')->getSellerProfileDisplayFlag()) {
            $this->getRequest()->initForward();
            $this->getRequest()->setActionName('noroute');
            $this->getRequest()->setDispatched(false);
            return false;
        }
		$id = 0;
		$profileurl = Mage::helper('marketplace')->getFeedbackUrl();
		if($profileurl){
			$storeId = Mage::app()->getStore()->getId();
			$data=Mage::getModel('marketplace/userprofile')->getCollection()
						->addFieldToFilter('wantpartner',array('eq'=>1))
						->addFieldToFilter('profileurl',array('eq'=>$profileurl))
						->addFieldToFilter('store_id',array('eq'=>$storeId));
            if(!count($data)){
			    $data = Mage::getModel('marketplace/userprofile')->getCollection()
						->addFieldToFilter('wantpartner',array('eq'=>1))
						->addFieldToFilter('profileurl',array('eq'=>$profileurl))
						->addFieldToFilter('store_id', 0);
			}
			foreach($data as $seller){ 
				$id =$seller->getAutoid();
			}
		}
		if($id){
			$this->loadLayout();     
			$this->renderLayout();
		}else{
			$this->_redirect("marketplace/index");
		}
	}
	public function usernameverifyAction(){
		$profileurl=$this->getRequest()->getParam('profileurl');
		$collection=Mage::getModel('marketplace/userprofile')->getCollection()
							->addFieldToFilter('profileurl',array('eq'=>$profileurl));
		$this->getResponse()->setHeader('Content-type', 'text/html');
		$this->getResponse()->setBody(count($collection));
	}
	public function sendmailAction(){
		if (!Mage::helper('marketplace')->getSellerProfileDisplayFlag()) {
            $this->getRequest()->initForward();
            $this->getRequest()->setActionName('noroute');
            $this->getRequest()->setDispatched(false);
            return false;
        }	
		$data = $this->getRequest()->getParams();
		if($data['seller-id']){
			Mage::dispatchEvent('mp_send_querymail', $data);
			if(!isset($data['product-id'])){
				$data['product-id'] = 0 ;
			}
			if($data['product-id']){
				$emailTemplate = Mage::helper('marketplace')->getQuerypartnerEmailTemplate();
			}
			else{
				$emailTemplate = Mage::helper('marketplace')->getAskQuerypartnerEmailTemplate();
			}
			if(Mage::helper('customer')->isLoggedIn()){
				$buyer_name = Mage::getSingleton('customer/session')->getCustomer()->getName();
				$buyer_email = Mage::getSingleton('customer/session')->getCustomer()->getEmail();
			}else{				
				$buyer_email = $data['email'];
				$buyer_name = $data['name'];
				if(strlen($buyer_name)<2){
					$buyer_name="Guest";
				}
			}
			$emailTemplateVariables = array();
			$seller=Mage::getModel('customer/customer')->load($data['seller-id']);
			$emailTemplateVariables['myvar1'] =$seller->getName();
			$sellerEmail = $seller->getEmail();
			$emailTemplateVariables['myvar3'] =Mage::getModel('catalog/product')->load($data['product-id'])->getName();
			$emailTemplateVariables['myvar4'] =$data['ask'];
			$emailTemplateVariables['myvar6'] =$data['subject'];
			$emailTemplateVariables['myvar5'] =$buyer_email;
			
			$processedTemplate = $emailTemplate->getProcessedTemplate($emailTemplateVariables);
			$emailTemplate->setSenderName($buyer_name);
			$emailTemplate->setSenderEmail($buyer_email);
			$emailTemplate->send($sellerEmail,$seller->getName(),$emailTemplateVariables);
		}
		$this->getResponse()->setHeader('Content-type', 'text/html');
		$this->getResponse()->setBody(json_encode("true"));
    }
	public function newfeedbackAction(){
		if (!Mage::helper('marketplace')->getSellerProfileDisplayFlag()) {
            $this->getRequest()->initForward();
            $this->getRequest()->setActionName('noroute');
            $this->getRequest()->setDispatched(false);
            return false;
        }
		if (!$this->_validateFormKey()) {
           return $this->_redirect('marketplace/marketplaceaccount/myproductslist/');
        }
		$wholedata=$this->getRequest()->getPost();
		Mage::getModel('marketplace/feedback')->saveFeedbackdetail($wholedata);
		Mage::getSingleton('core/session')->addSuccess(Mage::helper('marketplace')->__('Your Review was successfully saved'));
		$this->_redirect("marketplace/seller/feedback/".$wholedata['profileurl'].'/.');		
	}
}
