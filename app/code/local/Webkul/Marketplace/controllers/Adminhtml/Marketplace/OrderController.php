<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Marketplace
 * @author    Webkul
 * @copyright Copyright (c) 2010-2016 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

class Webkul_Marketplace_Adminhtml_Marketplace_OrderController extends Mage_Adminhtml_Controller_Action
{
    protected function _isAllowed(){
		return Mage::getSingleton('admin/session')->isAllowed('admin/marketplace/marketplace_order');
	}
    
	protected function _initAction() {		
		$this->_title($this->__("Manage Seller's Order"));
		$this->loadLayout()
			->_setActiveMenu('marketplace/marketplace_order')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Items Manager'), Mage::helper('adminhtml')->__('Item Manager'));
		
		return $this;
	}   
 
	public function indexAction() {
		$this->_initAction()
			->renderLayout();
	}
	public function gridAction(){
        $this->loadLayout();
        $this->getResponse()->setBody($this->getLayout()->createBlock("marketplace/adminhtml_order_grid")->toHtml()); 
    }
    public function masspayAction(){    	
		$wholedata=$this->getRequest()->getParams();		
		$actparterprocost = 0;
		$totalamount = 0;
		$seller_id = $wholedata['sellerid'];
		if (!$this->_validateFormKey()) {
         	$this->_redirect('adminhtml/marketplace_order/index',array('id'=>$seller_id));
        }
		$wksellerorderids = explode(',',$wholedata['wksellerorderids']);
		$orderinfo = '';
		$style='style="font-size:11px;padding:3px 9px;border-bottom:1px dotted #cccccc";';
		$tax_to_seller = Mage::helper('marketplace')->getConfigTaxMange();
		foreach($wksellerorderids as $key){
			$collection = Mage::getModel('marketplace/saleslist')->getCollection()
									->addFieldToFilter('autoid',array('eq'=>$key))
									->addFieldToFilter('cpprostatus',array('eq'=>1))
									->addFieldToFilter('paidstatus',array('eq'=>0))
									->addFieldToFilter('mageorderid',array('neq'=>0));
			foreach ($collection as $row) {
				$order=Mage::getModel('sales/order')->load($row['mageorderid']);
				$tax_amount = $row['totaltax'];
				$marketplace_orders = Mage::getModel('marketplace/order')->getCollection()
		                            ->addFieldToFilter('order_id',$row['mageorderid'])
		                            ->addFieldToFilter('seller_id',$row['mageproownerid']);
				foreach ($marketplace_orders as $tracking) {
					$tax_to_seller=$tracking['tax_to_seller'];
				}
				$vendor_tax_amount = 0;
				if($tax_to_seller){
					$vendor_tax_amount = $tax_amount;
				}
				$cod_charges = 0;
				$shipping_charges = 0;
				$cod_charges = $row->getCodCharges();
				if($row->getIsShipping()==1){
					$marketplace_orders = Mage::getModel('marketplace/order')->getCollection()
			                            ->addFieldToFilter('order_id',$row['mageorderid'])
			                            ->addFieldToFilter('seller_id',$row['mageproownerid']);
					foreach ($marketplace_orders as $tracking) {
						$shippingamount=$tracking->getShippingCharges();
						$refundedShippingAmount=$tracking->getRefundedShippingCharges();
						$shipping_charges = $shippingamount - $refundedShippingAmount;
					}
				}
				$actparterprocost = $actparterprocost+$row->getActualparterprocost()+$vendor_tax_amount+$cod_charges+$shipping_charges;
				$totalamount = $totalamount + $row->getTotalamount()+$tax_amount+$cod_charges+$shipping_charges;
				$seller_id = $row->getMageproownerid();
				$orderinfo = $orderinfo."<tr>
								<td valign='top' align='left' ".$style.">".$row['magerealorderid']."</td>
								<td valign='top' align='left' ".$style.">".$row['mageproname']."</td>
								<td valign='top' align='left' ".$style.">".$row['magequantity']."</td>
								<td valign='top' align='left' ".$style.">".$order->formatPrice($row['mageproprice'])."</td>
								<td valign='top' align='left' ".$style.">".$order->formatPrice($row['totalcommision'])."</td>
								<td valign='top' align='left' ".$style.">".$order->formatPrice($row['actualparterprocost'])."</td>
					 		</tr>";
			}
		}
		if($actparterprocost){		
			$collectionverifyread = Mage::getModel('marketplace/saleperpartner')->getCollection();
			$collectionverifyread->addFieldToFilter('mageuserid',array('eq'=>$seller_id));
			if(count($collectionverifyread)>=1){
				foreach($collectionverifyread as $verifyrow){
					if($verifyrow->getAmountremain() >= $actparterprocost){
						$totalremain=$verifyrow->getAmountremain()-$actparterprocost;
					}
					else{
						$totalremain=0;
					}
					$verifyrow->setAmountremain($totalremain);
					$verifyrow->save();
					$totalremain;
					$amountpaid=$verifyrow->getAmountrecived();
					$totalrecived=$actparterprocost+$amountpaid;
					$verifyrow->setAmountpaid($actparterprocost);
					$verifyrow->setAmountrecived($totalrecived);
					$verifyrow->setAmountremain($totalremain);
					$verifyrow->save();
				}
			}
			else{
				$percent = Mage::helper('marketplace')->getConfigCommissionRate();			
				$collectionf=Mage::getModel('marketplace/saleperpartner');
				$collectionf->setMageuserid($seller_id);
				$collectionf->setTotalsale($totalamount);
				$collectionf->setAmountpaid($actparterprocost);
				$collectionf->setAmountrecived($actparterprocost);
				$collectionf->setAmountremain(0);
				$collectionf->setCommision($percent);
				$collectionf->save();						
			}

			$unique_id = $this->checktransid();
			if($unique_id!=''){
				$seller_trans = Mage::getModel('marketplace/sellertransaction')->getCollection()
	                    ->addFieldToFilter('transactionid',array('eq'=>$unique_id));            
	            if(count($seller_trans)){
					foreach ($seller_trans as $value) {
						$id =$value->getId();
						if($id){
							Mage::getModel('marketplace/sellertransaction')->load($id)->delete();
						}
			    	}
				}
				$currdate = date('Y-m-d H:i:s');
				$seller_trans = Mage::getModel('marketplace/sellertransaction');
				$seller_trans->setTransactionid($unique_id);
				$seller_trans->setTransactionamount($actparterprocost);
				$seller_trans->setType('Manual');
				$seller_trans->setMethod('Manual');
				$seller_trans->setSellerid($seller_id);
				$seller_trans->setCustomnote($wholedata['customnote']);
				$seller_trans->setCreatedAt($currdate);
				$transid = $seller_trans->save()->getTransid();
			}

			foreach($wksellerorderids as $key){
				$collection = Mage::getModel('marketplace/saleslist')->getCollection()
										->addFieldToFilter('autoid',array('eq'=>$key))
										->addFieldToFilter('cpprostatus',array('eq'=>1))
										->addFieldToFilter('paidstatus',array('eq'=>0))
										->addFieldToFilter('mageorderid',array('neq'=>0));
				foreach ($collection as $row) {
					$row->setPaidstatus(1);
					$row->setIsPaid(1);
					$row->setTransid($transid)->save();
					$data['id']=$row->getMageorderid();
					$data['seller_id']=$row->getMageproownerid();
					Mage::dispatchEvent('mp_pay_seller', $data);
				}
			}

			$seller = Mage::getModel('customer/customer')->load($seller_id);	
			$emailTemp = Mage::helper('marketplace')->getSellerTransactionmailTemplate();
			
			$emailTempVariables = array();				
			$admin_storemail = Mage::helper('marketplace')->getAdminEmailId();
			$adminEmail=$admin_storemail? $admin_storemail:Mage::helper('marketplace')->getDefaultTransEmailId();
			$adminUsername = 'Admin';
			$emailTempVariables['myvar1'] = $seller->getName();
			$emailTempVariables['myvar2'] = $transid;
			$emailTempVariables['myvar3'] = $currdate;
			$emailTempVariables['myvar4'] = $actparterprocost;
			$emailTempVariables['myvar5'] = $orderinfo;
			$emailTempVariables['myvar6'] = $wholedata['customnote'];			
			$processedTemplate = $emailTemp->getProcessedTemplate($emailTempVariables);			
			$emailTemp->setSenderName($adminUsername);
			$emailTemp->setSenderEmail($adminEmail);
			$emailTemp->send($seller->getEmail(),$seller->getName(),$emailTempVariables);

			$this->_getSession()->addSuccess(Mage::helper('marketplace')->__('Payment has been successfully done for this seller'));
		}
		$this->_redirect('adminhtml/marketplace_order/index',array('id'=>$seller_id));
	}

	public function paysellerAction(){
		$wholedata=$this->getRequest()->getParams();		
		$actparterprocost = 0;
		$totalamount = 0;
		$seller_id = $wholedata['sellerid'];
		if (!$this->_validateFormKey()) {
         	$this->_redirect('adminhtml/marketplace_order/index',array('id'=>$seller_id));
        }
		$orderinfo = '';
		$totaltax_amount = 0;
		$total_shipping_charges = 0;
		$total_cod = 0;
		$order_id = 0;
		$style='style="font-size:11px;padding:3px 9px;border-bottom:1px dotted #cccccc";';
		$tax_to_seller = Mage::helper('marketplace')->getConfigTaxMange();
		$collection = Mage::getModel('marketplace/saleslist')->getCollection()
								->addFieldToFilter('autoid',array('eq'=>$wholedata['autoorderid']))
								->addFieldToFilter('cpprostatus',array('eq'=>1))
								->addFieldToFilter('paidstatus',array('eq'=>0))
								->addFieldToFilter('mageorderid',array('neq'=>0));
		foreach ($collection as $row) {
			$order_id = $row['mageorderid'];
			$order=Mage::getModel('sales/order')->load($order_id);
			$tax_amount = $row['totaltax'];
			$marketplace_orders = Mage::getModel('marketplace/order')->getCollection()
		                            ->addFieldToFilter('order_id',$row['mageorderid'])
		                            ->addFieldToFilter('seller_id',$row['mageproownerid']);
			foreach ($marketplace_orders as $tracking) {
				$tax_to_seller=$tracking['tax_to_seller'];
			}
			$vendor_tax_amount = 0;
			if($tax_to_seller){
				$vendor_tax_amount = $tax_amount;
			}
			$cod_charges = 0;
			$shipping_charges = 0;
			$cod_charges = $row->getCodCharges();
			if($row->getIsShipping()==1){
				$marketplace_orders = Mage::getModel('marketplace/order')->getCollection()
		                            ->addFieldToFilter('order_id',$row['mageorderid'])
		                            ->addFieldToFilter('seller_id',$row['mageproownerid']);
				foreach ($marketplace_orders as $tracking) {
					$shippingamount=$tracking->getShippingCharges();
					$refundedShippingAmount=$tracking->getRefundedShippingCharges();
					$shipping_charges = $shippingamount - $refundedShippingAmount;
				}
			}
			$actparterprocost = $actparterprocost+$row->getActualparterprocost()+$vendor_tax_amount+$cod_charges+$shipping_charges;
			$totalamount = $totalamount + $row->getTotalamount()+$tax_amount+$cod_charges+$shipping_charges;
			$seller_id = $row->getMageproownerid();

			$totaltax_amount = $totaltax_amount + $vendor_tax_amount;
			$total_shipping_charges = $total_shipping_charges + $shipping_charges;
			$total_cod = $total_cod + $cod_charges;
			$orderinfo = $orderinfo."<tr>
							<td valign='top' align='left' ".$style.">".$row['magerealorderid']."</td>
							<td valign='top' align='left' ".$style.">".$row['mageproname']."</td>
							<td valign='top' align='left' ".$style.">".$row['magequantity']."</td>
							<td valign='top' align='left' ".$style.">".$order->formatPrice($row['mageproprice'])."</td>
							<td valign='top' align='left' ".$style.">".$order->formatPrice($row['totalcommision'])."</td>
							<td valign='top' align='left' ".$style.">".$order->formatPrice($row['actualparterprocost'])."</td>
				 		</tr>";
		}

		$order = Mage::getModel('sales/order')->load($order_id);

		$orderinfo = $orderinfo."</tbody><tbody><tr style='font-size:11px;'>
									<td align='right' style='padding:3px 9px' colspan='3'>".Mage::helper('marketplace')->__('Shipping & Handling Charges')."</td>
									<td align='right' style='padding:3px 9px' colspan='3'><span>".$order->formatPrice($total_shipping_charges)."</span></td>
								</tr><tr style='font-size:11px;'>
									<td align='right' style='padding:3px 9px' colspan='3'>".Mage::helper('marketplace')->__('Tax Amount')."</td>
									<td align='right' style='padding:3px 9px' colspan='3'><span>".$order->formatPrice($totaltax_amount)."</span></td>
								</tr><tr>
									<td align='right' style='padding:3px 9px' colspan='3'>".Mage::helper('marketplace')->__('Total Vendor Amount')."</td>
									<td align='right' style='padding:3px 9px' colspan='3'><span>".$order->formatPrice($actparterprocost)."</span></td>
								</tr>";
		if($actparterprocost){		
			$collectionverifyread = Mage::getModel('marketplace/saleperpartner')->getCollection();
			$collectionverifyread->addFieldToFilter('mageuserid',array('eq'=>$seller_id));
			if(count($collectionverifyread)>=1){
				foreach($collectionverifyread as $verifyrow){
					if($verifyrow->getAmountremain() >= $actparterprocost){
						$totalremain=$verifyrow->getAmountremain()-$actparterprocost;
					}
					else{
						$totalremain=0;
					}
					$verifyrow->setAmountremain($totalremain);
					$verifyrow->save();
					$totalremain;
					$amountpaid=$verifyrow->getAmountrecived();
					$totalrecived=$actparterprocost+$amountpaid;
					$verifyrow->setAmountpaid($actparterprocost);
					$verifyrow->setAmountrecived($totalrecived);
					$verifyrow->setAmountremain($totalremain);
					$verifyrow->save();
				}
			}
			else{
				$percent = Mage::helper('marketplace')->getConfigCommissionRate();			
				$collectionf=Mage::getModel('marketplace/saleperpartner');
				$collectionf->setMageuserid($seller_id);
				$collectionf->setTotalsale($totalamount);
				$collectionf->setAmountpaid($actparterprocost);
				$collectionf->setAmountrecived($actparterprocost);
				$collectionf->setAmountremain(0);
				$collectionf->setCommision($percent);
				$collectionf->save();						
			}

			$unique_id = $this->checktransid();
			if($unique_id!=''){
				$seller_trans = Mage::getModel('marketplace/sellertransaction')->getCollection()
	                    ->addFieldToFilter('transactionid',array('eq'=>$unique_id));            
	            if(count($seller_trans)){
					foreach ($seller_trans as $value) {
						$id =$value->getId();
						if($id){
							Mage::getModel('marketplace/sellertransaction')->load($id)->delete();
						}
			    	}
				}
				$currdate = date('Y-m-d H:i:s');
				$seller_trans = Mage::getModel('marketplace/sellertransaction');
				$seller_trans->setTransactionid($unique_id);
				$seller_trans->setTransactionamount($actparterprocost);
				$seller_trans->setType('Manual');
				$seller_trans->setMethod('Manual');
				$seller_trans->setSellerid($seller_id);
				$seller_trans->setCustomnote($wholedata['seller_pay_reason']);
				$seller_trans->setCreatedAt($currdate);
				$transid = $seller_trans->save()->getTransid();
			}

			
			$collection = Mage::getModel('marketplace/saleslist')->getCollection()
							->addFieldToFilter('autoid',array('eq'=>$wholedata['autoorderid']))
							->addFieldToFilter('cpprostatus',array('eq'=>1))
							->addFieldToFilter('paidstatus',array('eq'=>0))
							->addFieldToFilter('mageorderid',array('neq'=>0));
			foreach ($collection as $row) {
				$row->setPaidstatus(1);
				$row->setIsPaid(1);
				$row->setTransid($transid)->save();
				$data['id']=$row->getMageorderid();
				$data['seller_id']=$row->getMageproownerid();
				Mage::dispatchEvent('mp_pay_seller', $data);
			}

			$seller = Mage::getModel('customer/customer')->load($seller_id);	
			$emailTemp = Mage::helper('marketplace')->getSellerTransactionmailTemplate();
			
			$emailTempVariables = array();				
			$admin_storemail = Mage::helper('marketplace')->getAdminEmailId();
			$adminEmail=$admin_storemail? $admin_storemail:Mage::helper('marketplace')->getDefaultTransEmailId();
			$adminUsername = Mage::helper('marketplace')->__('Admin');
			$emailTempVariables['myvar1'] = $seller->getName();
			$emailTempVariables['myvar2'] = $transid;
			$emailTempVariables['myvar3'] = $currdate;
			$emailTempVariables['myvar4'] = $actparterprocost;
			$emailTempVariables['myvar5'] = $orderinfo;
			$emailTempVariables['myvar6'] = $wholedata['seller_pay_reason'];		
			$processedTemplate = $emailTemp->getProcessedTemplate($emailTempVariables);			
			$emailTemp->setSenderName($adminUsername);
			$emailTemp->setSenderEmail($adminEmail);
			$emailTemp->send($seller->getEmail(),$seller->getName(),$emailTempVariables);

			$this->_getSession()->addSuccess(Mage::helper('marketplace')->__('Payment has been successfully done for this seller'));
		}
		$this->_redirect('adminhtml/marketplace_order/index',array('id'=>$seller_id));
	}

	public function randString($length, $charset='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789')
	{
	    $str = 'tr-';
	    $count = strlen($charset);
	    while ($length--) {
	        $str .= $charset[mt_rand(0, $count-1)];
	    }

	    return $str;
	}

	public function checktransid(){
		$unique_id=$this->randString(11);
		$collection = Mage::getModel('marketplace/sellertransaction')->getCollection()
                    ->addFieldToFilter('transactionid',array('eq'=>$unique_id));
        $i=0;
    	foreach ($collection as $value) {
    			$i++;
    	}   
    	if($i!=0){
            $this->checktransid();
        }else{
        	return $unique_id;
        }		
	}

    public function massApproveAction(){
		$orderIds = $this->getRequest()->getPost('order_ids', array());

        $status = 1;

        $helper = Mage::helper('marketplace');

        foreach ($orderIds as $orderId) {
            $orderPendingMailsCollection = Mage::getModel('marketplace/orderpendingemails')
            ->getCollection()
            ->addFieldToFilter('status', 0)
            ->addFieldToFilter(
                'order_id',
                $orderId
            );

            $order = Mage::getModel('sales/order')->load($orderId);

            foreach ($orderPendingMailsCollection as $key => $value) {
                $emailTemplateVariables = array();
                $emailTempVariables['myvar1'] = $value['myvar1'];
                $emailTempVariables['myvar2'] = $value['myvar2'];
                $emailTempVariables['myvar3'] = $value['myvar3'];
                $emailTempVariables['myvar4'] = $value['myvar4'];
                $emailTempVariables['myvar5'] = $value['myvar5'];
                $emailTempVariables['myvar6'] = $value['myvar6'];
                $emailTempVariables['myvar8'] = $value['myvar8'];
                $emailTempVariables['myvar9'] = $value['myvar9'];
                $emailTempVariables['isNotVirtual'] = $value['isNotVirtual'];

                $adminUsername = $value['sender_name'];
                $adminEmail = $value['sender_email'];

                $username = $value['receiver_name'];
                $useremail = $value['receiver_email'];

                /* load email template */
				$emailTemp = Mage::helper('marketplace')->getOrderPlaceNotifymailTemplate();
				$processedTemplate = $emailTemp->getProcessedTemplate($emailTempVariables);
				$emailTemp->setSenderName($adminUsername);
				$emailTemp->setSenderEmail($adminEmail);
				$emailTemp->send($useremail,$username,$emailTempVariables);

                $value->setStatus(1)->save();
                $order->setOrderApprovalStatus(1)->save();
            }
        }
        $this->_getSession()->addSuccess(
            __(
                'A total of %s record(s) have been approved.',
                count($orderIds)
            )
        );
        $this->_redirect('adminhtml/sales_order/index');
	}
}