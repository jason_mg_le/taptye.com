<?php

Class TinyApps_Customproduct_Model_Observer
{
    public function beforeCreateProduct($observer)
    {
        $product = $observer->getProduct();
        if (!$product->getData('qrcode'))
            $product->setData('qrcode', hash('sha256', time(), false));
        Mage::getModel('qrcode/qrcode')->GenerateProduct($product);
    }

}